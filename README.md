# InsuranceManager : Angular 9
#### By Mark Webley

#### Search for insurance for all criterias 
using just one text field, instead of multiple dropmenus etc

###### ✓ Code quality
I have included example of basic architecture, class structure, and advanced services, no backend service was built because none was required & asked for this test :). Actually normally I deliver added value, but the last time I did this and created a front end and backend, the test guys said I delivered too much so used it an excuse to fail a test, which I do not aggree with. In England we value added-value, it is logical to us.

###### ✓ Mobile support
I included example code of mobile first manually coded with @media queries. Angular Material UI is used to get the basic structure up and running.

###### ✓ Application structure
I created a module component that is lazy loaded, with its individual components, to know more about enterprise architecture, call me to discuss.

###### ✓ Unit testing
Two example unit tests are included that is important for any development, the component and the services.

services:       insurance-manager\src\app\services\products.service.spec.ts

ListComponent:  insurance-manager\src\app\mw-insurance\components\list\list.component.spec.ts

###### ✓ User Interface
I could have used bootstrap, but I used Angular Material, because of its mobile capabilities.
In the real world, I use both manuel developement with either Bootstrap or Angular Material, with manual styles developed in sass,
I can use pure css3, but sass saves me time, means I do more with less code, and less code duplication.

###### ✓ Optimization (load times and rendering performance)
I have created a module that is lazy loaded, there is more that I do for architecture, and data structures, call me to discuss.



#### Angular 9 code by Mark Webley,
run: npm i

#####  then run: 
npm run start,

#####  Then 
then visit: http://localhost:4501/insurance


#### unit test:
services:       insurance-manager\src\app\services\products.service.spec.ts
ListComponent:  insurance-manager\src\app\mw-insurance\components\list\list.component.spec.ts

#### modular architecture
mw-insurance

#### responsive web using
angular material and @media queries


### unit test: services
![Alt text](screenshots/product-service-test.png "Product service component unit test")

### unit test: ListComponent
![Alt text](screenshots/list-component-test.png "List componennt unit test")

### Mobile view filtered search
![Alt text](screenshots/mobile-view-filtered-search.png "mobile view filtered search")

### Mobile view
![alt-text-1](screenshots/mobile-view.png "mobile view 1") ![alt-text-2](screenshots/mobile-view-2.png "mobile view two")

### Desktop view filtered search
![Alt text](screenshots/desktop-view-filtered-search.png "desktop view filtered search")

### Desktop view standard & with pagination options
![Alt text](screenshots/desktop-view-pagaination-options.png "desktop view pagination options")

