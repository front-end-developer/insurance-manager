/**
 * Created by Mark Webley on 29/03/2020.
 */
export interface ProductDetail {
  id: string;
  name: string;
  brand: string;
  'brand-image': string;
  kind: string;
  'Kind-image': string;
  price: string;
}

export interface ProductList {
  productList: ProductDetail[];
}
