import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Subject, of } from 'rxjs';
import { ListComponent } from './list.component';
import {ProductService} from '../../../services/products.service';
import {CommonModule} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MaterialModule} from '../common/material/material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

const MockResponse = [{
    id:           '1',
    name:         'Casa mia!',
    brand:        'Generali',
    'brand-image':  'brand_generali.png',
    kind:         'Hogar',
    'Kind-image':   'kind_home.png',
    price:        '300'
  }, {
    id:           '2',
    name:         'No eres imortal',
    brand:        'Mapfre',
    'brand-image':  'brand_mapfre.png',
    kind:         'Vida',
    'Kind-image':   'kind_life.png',
    price:        '100'
  }];

const MockProductService$ = new Subject<any>();
const MockProductService = {
  getProductList : jasmine.createSpy('getProductList').and.returnValue(MockProductService$.asObservable())
};

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComponent ],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        MaterialModule,
        NoopAnimationsModule
      ],
      providers: [
        { provide: ProductService, useValue: MockProductService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    MockProductService.getProductList.calls.reset();
  });

  it('should create ListComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should loadProducts', () => {
    MockProductService$.next(MockResponse);
    component.loadProducts();
    expect(MockProductService.getProductList).toHaveBeenCalled();
    expect(component.productList).not.toBeNull();
    expect (component.productList.length).toEqual(2);
  });
});
