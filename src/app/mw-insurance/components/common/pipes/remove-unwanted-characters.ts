/**
 * Created by Mark Webley on 06/04/2020.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeUnwantedCharactersPipe'
})

export class RemoveUnwantedCharactersPipe implements PipeTransform {
  transform(value: string): any {
    return value.replace(/[<>\\]|[\[\]]|[\?$#!]/g, ' ').replace(/\s\s/, ' '); // <.*?>
  }
}

// "ESC\#??$.<asdf>[SHIFT][RUN][ENTER]8.32!Out of memory".replace(/<.*?>|[\[\]]|[\?$#!]/g, '')
