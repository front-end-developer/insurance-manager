import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MwInsuranceRoutingModule } from './mw-insurance-routing.module';
import { MwInsuranceComponent } from './mw-insurance.component';
import { ListComponent } from './components/list/list.component';
import {MaterialModule} from './components/common/material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RemoveUnwantedCharactersPipe} from './components/common/pipes/remove-unwanted-characters';


@NgModule({
  declarations: [
    RemoveUnwantedCharactersPipe,
    MwInsuranceComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MwInsuranceRoutingModule
  ]
})
export class MwInsuranceModule { }
