import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MwInsuranceComponent } from './mw-insurance.component';

const routes: Routes = [{ path: '', component: MwInsuranceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MwInsuranceRoutingModule { }
