import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MwInsuranceComponent } from './mw-insurance.component';

describe('MwInsuranceComponent', () => {
  let component: MwInsuranceComponent;
  let fixture: ComponentFixture<MwInsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MwInsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MwInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
