/**
 * Created by Mark Webley on 29/03/2020.
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductDetail} from '../models/product-list/product-list';
// @ts-ignore
import * as productsData from '../mocks/products_GET.json';
import {Observable, of} from 'rxjs';

/**
 * @alias property.service.ts
 */
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private tmpProductsCache = [];

  constructor(
    private http: HttpClient
  ) { }

  /**
   * @description return a list of all products
   *
   * @readOnly
   * @memberOf ProductService
   */
  getProductList(): Observable<ProductDetail[]> {
    return of<ProductDetail[]>(productsData);
  }
}
