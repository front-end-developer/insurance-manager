import { TestBed, getTestBed } from '@angular/core/testing';
import { ProductService } from './products.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ProductService', () => {
  let injector: TestBed;
  let service: ProductService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    injector = getTestBed();
    service = injector.get(ProductService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getProductList function called ', () => {
    service.getProductList();
    spyOn(service, 'getProductList').call(this);
    expect(service.getProductList).toHaveBeenCalled();
  });
});
