import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'insurance',
    loadChildren: () => import('./mw-insurance/mw-insurance.module').then(m => m.MwInsuranceModule)
  },
  {
    path: '',
    redirectTo: '/insurance',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
